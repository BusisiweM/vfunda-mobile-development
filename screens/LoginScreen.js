import React , {useState} from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';

import  FormInput  from "../components/FormInput";
import  FormButton  from "../components/FormButton";
import  SocialButton from "../components/SocialButton";

import { firebase } from '../firebase/config'

function LoginScreen({navigation}) {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    
    const onLoginPress = () => {
        firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then((response) => {
                const uid = response.user.uid
                const usersRef = firebase.firestore().collection('users')
                usersRef
                    .doc(uid)
                    .get()
                    .then(firestoreDocument => {
                        if (!firestoreDocument.exists) {
                            alert("User does not exist anymore.")
                            return;
                        }
                        const user = firestoreDocument.data()
                        navigation.navigate('Home', {user: user})
                    })
                    .catch(error => {
                        alert(error)
                    });
            })
            .catch(error => {
                alert(error)
            })
    }


    return (
        <View style = {styles.container}>
           <Image
                source= {require('../assets/v1.jpg')}
                style = {styles.logo}
           />
           
           <Text style = {styles.text}> Vfunda App</Text>

            <FormInput 
                labelValue={email}
                onChangeText={(userEmail) => setEmail(userEmail)}
                placeholderText="Email"
                iconType="user"
                keyboardType= "email-address"
                autoCapitalize="none"
                autoCorrect={false}
              
           />

            <FormInput 
                labelValue={password}
                onChangeText={(userPassword) => setPassword(userPassword)}
                placeholderText="password"
                iconType="lock"
                secureTextEntry={true}
           />

           <FormButton
                buttonTitle= "Sign In"
                onPress={() => onLoginPress()}
           />

           <TouchableOpacity style={styles.forgotButton} onPress={() => {}} >
                <Text style={styles.navButtonText}>Forgot Password</Text>
           </TouchableOpacity>

           <SocialButton
                buttonTitle="Sign In with Google"
                btnType="google"
                color="#de4d41"
                backgroundColor="#f5e7ea"
                onPress = {() => {}}
           />

           <TouchableOpacity style={styles.forgotButton} onPress={() => navigation.navigate('Signup')} >
                <Text style={styles.navButtonText}>Don't have an Account? Create here</Text>
           </TouchableOpacity>  
        </View>
    );
};

export default LoginScreen;

const styles = StyleSheet.create({
    container: {
       backgroundColor: '#f9fafd',
       flex: 1,
       justifyContent: 'center',
       alignItems: 'center',
       padding: 20,
    },

    logo: {
        height: 150,
        width: 150,
        resizeMode: 'cover',
    },
    text: {
        fontSize: 28,
        marginBottom: 10,
        color: '#051d5f',
    },
    navButton: {
        marginTop: 15,

    },
    forgotButton: {
        marginVertical: 53,

    },
    navButtonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#2e64e5',
    },
});