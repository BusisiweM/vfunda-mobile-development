import * as firebase from 'firebase';

import '@firebase/auth';
import '@firebase/firestore';

const firebaseConfig = {
   apiKey: "AIzaSyDz5TaSpM0H6M4wGdLbcz76v5QXZ-PrN0w",
    authDomain: "vfunda1-b4e05.firebaseapp.com",
    projectId: "vfunda1-b4e05",
    storageBucket: "vfunda1-b4e05.appspot.com",
    messagingSenderId: "466880216150",
    appId: "1:466880216150:web:3cb3b06406f9471a994ff7"

};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export { firebase };
