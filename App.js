import 'react-native-gesture-handler';
import {View} from 'react-native';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import OnboardingScreen from './screens/OnboardingScreen';
import LoginScreen from './screens/LoginScreen';
import SignupScreen from './screens/SignupScreen';
import HomeScreen from './screens/HomeScreen';



const AppStack = createStackNavigator();



function App() {
  
  return (
    <NavigationContainer>
      <AppStack.Navigator
      headerMode = "none"
      >
        <AppStack.Screen name= "Onboarding" component={OnboardingScreen} />
        <AppStack.Screen name= "Login" component={LoginScreen} />
        <AppStack.Screen name= "Signup" component= {SignupScreen}
          options = {({navigation}) => ({
            title: '',
            headerStyle:{
              backgroundColor: '#f9fafd',
              shadowColor:'#f9fafd',
              elavation: 0,
            },
            headerLeft: () => (
                <View style={{marginRight: 10}}>
                  <FontAwesome.Button
                  name="long-arrow-left"
                  size = {25}
                  backgroundColor = "#f9fafd"
                  color= "#333"
                  onPress = {() => navigation.navigate('Login')}

                  />
                </View>
            ),
          })}/>
          <AppStack.Screen name= "Home" component={HomeScreen} />
      </AppStack.Navigator>
    </NavigationContainer>
  );
}

export default App;


